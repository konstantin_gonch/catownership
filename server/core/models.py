from django.db import models
from django.contrib.auth.models import User


class Cat(models.Model):
    BREEDS = ((1, "Мейн-кун"), (2, "Сфинкс"), (3, "Манчкин"), (4, "Рэгдолл"), (5, "Бурма"), (6, "Мэнкс"))

    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    age = models.PositiveIntegerField()
    breed = models.CharField(max_length=50, choices=BREEDS)
    photo = models.ImageField()

